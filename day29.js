function sortWord(word) {
  return word.split("").sort().join("");
}

function groupAnagrams(words) {
  const anagramMap = new Map();

  words.forEach((word) => {
    const sortedWord = sortWord(word);
    if (anagramMap.has(sortedWord)) {
      anagramMap.get(sortedWord).push(word);
    } else {
      anagramMap.set(sortedWord, [word]);
    }
  });

  const groupedAnagrams = Array.from(anagramMap.values());
  return groupedAnagrams;
}

function printGroupedAnagrams(groupedAnagrams) {
  groupedAnagrams.forEach((anagrams) => {
    console.log(anagrams.join(" "));
  });
}

const words = [
  "listen",
  "silent",
  "enlist",
  "hello",
  "world",
  "hit",
  "thi",
  "it",
];

const groupedAnagrams = groupAnagrams(words);
printGroupedAnagrams(groupedAnagrams);
