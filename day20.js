function minSwap(arr,  n,  k) {
    var count = 0;
    for (var i = 0; i < n; ++i)
        if (arr[i] <= k)
            ++count;
    var bad = 0;
    for (var i = 0; i < count; ++i)
        if (arr[i] > k)
            ++bad;

    var ans = bad;
    for (var i = 0, j = count; j < n; ++i, ++j) {
         
        // Decrement count of previous window
        if (arr[i] > k)
            --bad;
         
        // Increment count of current window
        if (arr[j] > k)
            ++bad;
         
        // Update ans if count of 'bad'
        // is less in current window
        ans = Math.min(ans, bad);
    }
    return ans;
}
 