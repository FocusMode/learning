var firstWord = "Below";
var secondWord = "Elbow";

isAnagram(firstWord, secondWord);

function isAnagram(first, second) {
  // Changing both words to lowercase for case sensitive.
  var a = first.toLowerCase();
  var b = second.toLowerCase();

  // Sorting the strings
  // Joining the resulting array to a string.
  
  a = a.split("").sort().join("");
  b = b.split("").sort().join("");
  
  // Finally Comparing the results
    
  return a === b;
}
