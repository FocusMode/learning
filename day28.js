// Find the next greater number with same sets of digits
function findNextGreaterNumber(num) {
    // Convert number to an array of digits
    let digits = Array.from(String(num), Number);
    
    // Step 1: Find the pivot
    let pivotIndex = -1;
    for (let i = digits.length - 2; i >= 0; i--) {
      if (digits[i] < digits[i + 1]) {
        pivotIndex = i;
        break;
      }
    }
    
    // Step 2: If there is no pivot, return null
    if (pivotIndex === -1) {
      return null;
    }
    
    // Step 3: Find the swap
    let swapIndex = -1;
    for (let i = digits.length - 1; i > pivotIndex; i--) {
      if (digits[i] > digits[pivotIndex]) {
        swapIndex = i;
        break;
      }
    }
    
    // Step 4: Swap the pivot and swap digits
    [digits[pivotIndex], digits[swapIndex]] = [digits[swapIndex], digits[pivotIndex]];
    
    // Step 5: Sort the digits to the right of the pivot
    let rightSide = digits.splice(pivotIndex + 1);
    rightSide.sort((a, b) => a - b);
    
    // Step 6: Concatenate the digits
    digits = digits.concat(rightSide);
    
    // Step 7: Convert back to number
    let nextGreater = parseInt(digits.join(''));
    
    return nextGreater;
  }