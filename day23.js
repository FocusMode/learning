// JavaScript program to find the largest
// palindromic subsequence

// Function to find the largest
// palindromic subsequence
function largestPalinSub(s) {
  let res = "";
  let mx = s[0];

  // Find the largest character
  for (let i = 1; i < s.length; i++)
    mx = String.fromCharCode(Math.max(mx.charCodeAt(), s[i].charCodeAt()));

  // Append all occurrences of largest
  // character to the resultant string
  for (let i = 0; i < s.length; i++) if (s[i] == mx) res += s[i];

  return res;
}
