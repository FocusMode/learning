// JS Program to Swap characters in a String
function swapCharacters(s, B, C) {
    const N = s.length;
    
    C = C % N;
     
    for (let i = 0; i < B; i++) {
        const temp = s[i];
        s = s.substring(0, i) + s[(i + C) % N] + s.substring(i + 1);
        s = s.substring(0, (i + C) % N) + temp + s.substring((i + C) % N + 1);
    }
    return s;
}
 
let s = "ABCDEFGH";
const B = 4;
const C = 3;
s = swapCharacters(s, B, C);
console.log(s);