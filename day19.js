// Union of two sorted array
function unionArray (arr1, arr2)
{
 
  // Create a set to store unique elements from both arrays
  const set1 = new Set(arr1);
  const set2 = new Set(arr2);
   
  // Merge both sets and convert back to array
  const result = [...new Set([...set1, ...set2])];
  return result;
}
 
// Driver code
const arr1 = [3,4,5,6];
const arr2 = [8,9,10,11,12,13,13];
 
// Function call
const uni = unionArray(arr1, arr2);
console.log(uni.join(' ')); // Output: 1 2 3 4 5