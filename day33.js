function combinationUtil(arr,data,start,end,index,r)
{
    // Current combination is ready to be printed, print it
    if (index == r)
    {
        for (let j=0; j<r; j++)
        {
            document.write(data[j]+" ");
        }
        document.write("<br>")
    }
     
    // replace index with all possible elements. The condition
    // "end-i+1 >= r-index" makes sure that including one element
    // at index will make a combination with remaining elements
    // at remaining positions
    for (let i=start; i<=end && end-i+1 >= r-index; i++)
    {
        data[index] = arr[i];
        combinationUtil(arr, data, i+1, end, index+1, r);
    }
}
 