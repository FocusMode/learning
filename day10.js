function separateLetters(str) {
    let vowels = "";
    let consonants = "";
    for (let i = 0; i < str.length; i++) {
      let letter = str[i].toLowerCase();
      if ("aeiou".includes(letter)) {
        vowels += letter;
      } else if (letter >= "a" && letter <= "z") {
        consonants += letter;
      }
    }
    return { vowels, consonants };
}
  
console.log(separateLetters("I am loving this challenge!!")); 
// { vowels: 'iaoiiaee', consonants: 'mlvngthschllng' }