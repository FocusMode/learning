function firstRepeatedWord (sentence) {
    // splitting the string
    let lis = sentence.split(" ");
     
    // Calculating frequency of every word
    let frequency = {};
    for (let i = 0; i < lis.length; i++) {
        let word = lis[i];
        if (!frequency[word]) {
            frequency[word] = 1;
        } else {
            frequency[word]++;
        }
    }
     
    // Traversing the list of words
    for (let i = 0; i < lis.length; i++) {
        let word = lis[i];
       
        // checking if frequency is greater than 1
        if (frequency[word] > 1) {
            // return the word
            return word;
        }
    }
}