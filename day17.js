function findKthMinMax(arr, k) {
    // Sort the array in ascending order
    arr.sort((a, b) => a - b);
  
    // Find the Kth minimum element
    const kthMin = arr[k - 1];
  
    // Sort the array in descending order
    arr.sort((a, b) => b - a);
  
    // Find the Kth maximum element
    const kthMax = arr[k - 1];
  
    return { kthMin, kthMax };
  }
  
  // Example usage
  const array = [5, 2, 9, 10, 1, 8];
  const k = 3;
  
  const result = findKthMinMax(array, k);
  console.log(`Kth minimum element: ${result.kthMin}`);
  console.log(`Kth maximum element: ${result.kthMax}`);