function characterReplacement(s, k) {
    let maxCount = 0;
    let maxLength = 0;
    let start = 0;
    const charCount = new Array(26).fill(0);
  
    for (let end = 0; end < s.length; end++) {
      const index = s.charCodeAt(end) - 'A'.charCodeAt(0);
      charCount[index]++;
  
      maxCount = Math.max(maxCount, charCount[index]);
  
      if (end - start + 1 - maxCount > k) {
        const startIndex = s.charCodeAt(start) - 'A'.charCodeAt(0);
        charCount[startIndex]--;
        start++;
      }
  
      maxLength = Math.max(maxLength, end - start + 1);
    }
  
    return maxLength;
  }
  
  // Example usage
  const str = "AABABBA";
  const k = 1;
  const longestLength = characterReplacement(str, k);
  console.log("Longest repeating character replacement:", longestLength);
  