// Valid Parentheses solution in Javascript
var isValid = function(s) {
    const parenthesis = {
        '(' : ')',
        '{' : '}',
        '[' : ']',
    };
    
    const stack = [];
    for(let i = 0; i < s.length; i++) {
    	const charParen = s[i];
    
        if (Object.keys(parenthesis).includes(charParen)) {
        	stack.push(charParen)
        } else {
        	const startParen = Object.keys(parenthesis).find(key => parenthesis[key] === charParen); 
        	if (stack?.[stack.length - 1] === undefined) {
            	return false;
            }
        
            if (stack?.[stack.length - 1] === startParen) {
        		stack.pop();
        	} else {
            	return false;
            }
        }
    }
    
    if (stack.length === 0) {
    	return true;
    }
    
    return false;
};