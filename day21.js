// Minimum characters to be added at front to make string palindrome
		// JavaScript Program to implement
		// the above approach

		// function for checking string is palindrome or not
		function ispalindrome(s) {
			let l = s.length;
			let j;

			for (let i = 0, j = l - 1; i <= j; i++, j--) {
				if (s[i] != s[j])
					return false;
			}
			return true;
		}

		// Driver code
        let s = "BABABAA";
        let cnt = 0;
        let flag = 0;
 
        while (s.length > 0)
        {
         
            // if string becomes palindrome then break
            if (ispalindrome(s)) {
                flag = 1;
                break;
            }
            else {
                cnt++;
 
                // erase the last element of the string
                s = s.substring(0, s.length - 1);
            }
        }
 
        // print the number of insertion at front
        if (flag)
            document.write(cnt);