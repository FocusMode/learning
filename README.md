# Learning

Day 1:Longest Substring Without Repeating Characters
Day 2: Practiced HTML/CSS
Day 3: Anagrams
Day 4: Remove the Vowels from a string
Day 5: Fibonacci Series Up to n Terms
Day 6: Factorial
Day 7: Program to find the HCF or GCD of two integers
Day 8: Program to find the LCM of two integers
Day 9: Reverse Array
Day 10: Separate Vowel and Consonant
Day 11: Check if a number is prime or not
Day 12: First Non-Repeating Character
Day 13: Swap Characters in a String
Day 14: Sort words in alphabetical order
Day 15: Most words found in a sentence
Day 16: Sorts an array that contains elements with values 0, 1, and 2. The sorting is done in such a way that all the 0s appear first in the array, followed by all the 1s, and finally, all the 2s.
Day 17: Find the "Kth" max and min element of an array
Day 18: Function to shift all the negative elements on left side
Day 19: Union of two sorted array
Day 20: Minimum swaps required to bring all elements less than or equal to k together
Day 21: Minimum characters to be added at front to make string palindrome
Day 22: Find the contiguous sub-array(containing at least one number) which has the maximum sum and return its sum.
Day 24: find the largest palindromic subsequence
**Sick Leave**
Day 27: find the first repeated word in the given string
Day 28: Find the next greater number with same sets of digits
Day 29: Print Anagrams Together
Day 30: Find the kth greatest element of a given array of integers
Day 31: Longest Repeating Character Replacement
Day 32: Valid Parentheses solution in Javascript
Day 33: print all combination of size r in an array of size n  