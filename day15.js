function mostWordsFound(sentences) {
    let ans = 0;
    for (let s of sentences) {
      let cnt = 1;
      for (let i = 0; i < s.length; ++i) {
        if (s.charAt(i) === ' ') {
          ++cnt;
        }
      }
      ans = Math.max(ans, cnt);
    }
    return ans;
  }