function shiftall(arr, left, right)
{
 
    while (left <= right)
    {
        if (arr[left] < 0 && arr[right] < 0)
            left += 1;
         
        else if(arr[left] > 0 && arr[right] < 0)
        {
            let temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left += 1;
            right -= 1;
        }         
        else if (arr[left] > 0 && arr[right] > 0)
            right -= 1;
        else
        {
            left += 1;
            right -= 1;
        }
    }
}
 
// Function to print the array
function display(arr, right)
{
     
    // Loop to iterate over the element
    // of the given array
    for(let i = 0; i < right; i++)
        document.write(arr[i] + " ");
}