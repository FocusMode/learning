// program to sort sentence in alphabetical order

const string = prompt('Enter a sentence: ');

const words = string.split(' ');

words.sort();

// display the sorted words
console.log('The sorted words are:');

for (const element of words) {
  console.log(element);
}